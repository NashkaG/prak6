import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Students> student = new ArrayList<>();
        student.add(new Students("Бабова",1,5,5,4,3,4));
        student.add(new Students("Гасанова",1,5,4,4,3,5));
        student.add(new Students("Губина",1,5,5,4,2,2));
        student.add(new Students("Скворцов",2,5,5,5,5,5));
        student.add(new Students("Глюкин",2,2,3,3,3,4));
        student.add(new Students("Глюкоза",2,3,5,3,4,4));
        student.add(new Students("Борисов",3,4,5,4,4,4));
        student.add(new Students("Самойлов",3,3,2,4,3,3));
        student.add(new Students("Светлячкова",3,4,4,3,2,2));
        student.add(new Students("Сразы",3,4,3,2,3,5));


        while(true) {
            System.out.println("1-Посмотреть студентов\n2-Посмотреть лучших студентов");
            Scanner scaner = new Scanner(System.in);
            int v = scaner.nextInt();
            switch (v){
                case 1:
                    student.sort(new Student());
                    Collections.reverse(student);
                    for(int i = 0; i < 10; i++)
                        System.out.println(student.get(i).Name + " Средний балл " + student.get(i).APoint);
                    break;
                case 2:
                    for(int i = 0; i < 10; i++)
                        if(student.get(i).APoint>=4)
                            System.out.println(student.get(i).Name + " Группа " + student.get(i).APoint);
                    break;

            }
        }
    }
}


/*

 for(int i = 0; i < 10; i++)
                        System.out.println(student.get(i).Name + " Группа " + student.get(i).GroupNum);
                    break;
 */