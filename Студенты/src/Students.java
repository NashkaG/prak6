public class Students {
    String Name;
    int GroupNum;
    double[] Marks = new double[5];
    double APoint;

    public double getAPoint() {
        return APoint;
    }

    public Students(String Name, int GroupNum, double Mark1, double Mark2, double Mark3, double Mark4, double Mark5){
        this.Name = Name;
        this.GroupNum = GroupNum;
        this.Marks[0] = Mark1;
        this.Marks[1] = Mark2;
        this.Marks[2] = Mark3;
        this.Marks[3] = Mark4;
        this.Marks[4] = Mark5;
        APoint =  (Marks[0] + Marks[1] + Marks[2] + Marks[3] + Marks[4])/5;
    }
}