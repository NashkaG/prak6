import java.util.Comparator;

public class Student implements Comparator<Students> {
    @Override
    public int compare(Students student1, Students student2){
        Double APoint1 = new Double(student1.getAPoint());
        Double APoint2 = new Double(student2.getAPoint());
        return APoint1.compareTo(APoint2);
    }
}
