public class Circle extends Figure {
    private double r;

    public Circle(double r){
        this.r = r;
    }

    public double getR() {
        return r;
    }

    @Override
    public double Perimeter() {
        return r *(2*Math.PI);
    }

    @Override
    public double Area() {
        return (r*Math.PI)*(r*Math.PI);
    }
}
