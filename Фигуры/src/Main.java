import java.util.*;
public class Main {
    public static void main(String[] args) {
        List<Figure> FigureList = new ArrayList<>();
        while(true){
            Scanner scan = new Scanner(System.in);
            System.out.println("1-Добавить\n2-Просмотр\n");
            int j = scan.nextInt();
            if (j > 2 || j < 0)
                break;
            switch (j){
                case 1:
                    System.out.println("Выберите фигуру: 1-Треугольник 2-Прямоугольник 3-Круг");
                    Scanner scaner = new Scanner(System.in);
                    int v = scaner.nextInt();
                    if (v < 1 || v > 3)
                        break;
                    switch (v){
                        case 1:
                            FigureList.add(new Triangle(Double.valueOf(Figure.Inputer("Первая сторона")),Double.valueOf(Figure.Inputer("Вторая сторона")),Double.valueOf(Figure.Inputer("Угол"))));
                            break;
                        case 2:
                            FigureList.add(new Rectangle(Double.valueOf(Figure.Inputer("Первая сторона")),Double.valueOf(Figure.Inputer("Вторая сторона"))));
                            break;
                        case 3:
                            FigureList.add(new Circle(Double.valueOf(Figure.Inputer("Радиус"))));
                            break;
                    }
                    break;
                case 2:
                    if (FigureList.size() == 0) {
                        System.out.println(FigureList.size());
                        System.out.println("Склад пуст");
                        continue;
                    }
                    Figure.FigureCheck(FigureList);
                    break;
            }
        }
    }
}