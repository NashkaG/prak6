import java.util.Scanner;

public class Book {

    int ID;
    String Name;
    String Author;
    int Year;

    public Book(int ID, String Name, String Author, int Year){
        this.ID = ID;
        this.Name = Name;
        this.Author = Author;
        this.Year = Year;
    }

    public static String Inputer(String message) {
        Scanner sc = new Scanner(System.in);
        System.out.println(message);
        return sc.next();
    }

}
